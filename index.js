/*Funcionalidade:
Seleciona o primeiro número(se escolher outro antes do operador, substitui o anterior)
Seleciona o operar(incapaz de mudar para outro operador, caso tente selecionar no template)
Seleciona o segundo número(Ao selecionar o segundo número, já aparece o resultado)
Não há funcionalidade para o botão de igual
Não há funcionalidade para o botão de decimal*/

var num1 = '';
var num2 = '';
var ope = '';
var resp = '';


function calc(elem) {
    var dis = document.getElementById("display");

    if((!(isNaN(elem)) || dis.innerHTML == "~") && ope == '' && num1 == '') {
        Peganum(elem);
        num1 = elem;
    }

    if(elem == "~") {
        document.getElementById("display").innerHTML = "~";
        num1 = '';
        num2 = '';
        ope = '';
    }

    if(isNaN(elem) && !(isNaN(num1)) && ope == '') {
        ope = elem;
        Peganum(ope);
    }

    if(ope != '' && !(isNaN(elem))) {
        Peganum(elem);
        num2 = elem;
    }

    if(num2 != '') {
        operar()
        document.getElementById("display").innerHTML = resp;
        num1 = resp;
        resp = '';
        num2 = '';
        ope = '';
    }
    
}

function Peganum(num) {
    var tela = document.getElementById("display");

    if(tela.innerHTML == "~" || (num1 != '' && ope =='' && num2 == '')){
        document.getElementById("display").innerHTML = num;
    }

    else {
        document.getElementById("display").innerHTML = tela.innerHTML + " " + num;
    }   
}

function operar() {
    switch (ope) {
        case '+' :
            resp = num1 + num2;
            break;
        case '-' :
            resp = num1 - num2;
            break;
        case '*' :
            resp = num1 * num2;
            break;
        case '/' :
            resp = num1 / num2;
            break;
    }
}